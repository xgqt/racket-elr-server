#!/bin/sh


# This file is part of racket-elr-server - ELisp package server in Racket.
# Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# racket-elr-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# racket-elr-server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with racket-elr-server.  If not, see <https://www.gnu.org/licenses/>.


set -e
trap 'exit 128' INT
export PATH


root_source="$(realpath "$(dirname "${0}")/../")"
racket_module="${root_source}"/src/elr-updater-lib/elr-server/updater/main.rkt

exec racket -- "${racket_module}" "${@}"
