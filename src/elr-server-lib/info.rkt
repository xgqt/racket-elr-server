#lang info


(define pkg-desc "ELisp package server in Racket. Server.")

(define version "0.0.0")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "elr-settings-lib"))
