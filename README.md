# ELR-Server

ELisp package server in Racket.


## About

ELR-Server project is an infrastructure to compile
and serve GNU Emacs packages via network protocols.
In the GNU Emacs jargon ELR-Server be called an "ELPA server".

As much components as possible are written in Racket
but some critical parts still use GNU Emacs.


## Requirements

- Racket compiled with the Chez backend --
  for the application server backend,

- SQLite, version 3 --
  for the application database,

- GNU Emacs --
  for compiling ELisp packages.


## Installation

### Raco

Use raco to install ELR-Server from the official Racket Package Catalog.

```sh
raco pkg install --auto --skip-installed --user elr
```

### Make

Use GNU Make to install Req from its project directory.

```sh
make install
```

### Req

Use Req to install ELR-Server from its project directory.

```sh
raco req --all --verbose
```


## Documentation

Documentation can either be built locally with `make public`
or browsed online on either [GitLab pages](https://xgqt.gitlab.io/racket-req/)
or [Racket-Lang Docs](https://docs.racket-lang.org/req/).


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
